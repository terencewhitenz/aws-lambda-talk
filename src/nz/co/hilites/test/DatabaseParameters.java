package nz.co.hilites.test;

import java.util.Map;

public class DatabaseParameters {
	
	private String dBEndPoint;
	private String dBDatabase;
	private String dBUser;
	private String dBPassword;

	public DatabaseParameters() {
		Map<String, String> env = System.getenv();
		dBEndPoint = env.get("DBEndPoint");
		if (dBEndPoint == null) {
			throw new RuntimeException("DBEndPoint parameter not set.");
		}
		dBDatabase = env.get("DBDatabase");
		if (dBDatabase == null) {
			throw new RuntimeException("DBDatabase parameter not set.");
		}
		dBUser = env.get("DBUser");
		if (dBUser == null) {
			throw new RuntimeException("DBUser parameter not set.");
		}
		dBPassword = env.get("DBPassword");
		if (dBPassword == null) {
			throw new RuntimeException("DBPassword parameter not set.");
		}
	}

	public String getdBEndPoint() {
		return dBEndPoint;
	}

	public String getdBDatabase() {
		return dBDatabase;
	}

	public String getdBUser() {
		return dBUser;
	}

	public String getdBPassword() {
		return dBPassword;
	}

}
