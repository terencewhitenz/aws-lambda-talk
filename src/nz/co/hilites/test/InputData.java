package nz.co.hilites.test;

public class InputData {
	
	private String currencyFrom;
	private float valueFrom;
	private String currencyTo;

	public String getCurrencyFrom() {
		return currencyFrom;
	}
	public void setCurrencyFrom(String currencyFrom) {
		this.currencyFrom = currencyFrom;
	}
	public float getValueFrom() {
		return valueFrom;
	}
	public void setValueFrom(float valueFrom) {
		this.valueFrom = valueFrom;
	}
	public String getCurrencyTo() {
		return currencyTo;
	}
	public void setCurrencyTo(String currencyTo) {
		this.currencyTo = currencyTo;
	}
	@Override
	public String toString() {
		return "InputData [" + (currencyFrom != null ? "currencyFrom=" + currencyFrom + ", " : "") + "valueFrom=" + valueFrom
				+ ", " + (currencyTo != null ? "currencyTo=" + currencyTo : "") + "]";
	}

}
