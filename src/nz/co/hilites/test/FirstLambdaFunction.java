package nz.co.hilites.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class FirstLambdaFunction implements RequestHandler<InputData, OutputData> {

	private Connection connection;
	private PreparedStatement select;
	private DatabaseParameters databaseParameters = new DatabaseParameters();
	// Database parameters are passed across using environment variables
	// See the DatabaseParameters bean for a list of parameters
	// The code will complain if the parameters are not set

	// TW - Static initialiser that is run once when the class is loaded by Lambda
	// Setting the database values once and connecting once saves time during each invocation which saves costs
	// This is a hint from an AWS youtube video on Lambda deep dive https://www.youtube.com/watch?v=dB4zJk_fqrU
	// Documentation for this is scarce
	{
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection("jdbc:mysql://" + databaseParameters.getdBEndPoint() + "/"
					+ databaseParameters.getdBDatabase() + "?user=" + databaseParameters.getdBUser() + "&password="
					+ databaseParameters.getdBPassword());
			select = connection.prepareStatement(
					"select conversion_rate from conversion_rates where currency_from = ? and currency_to = ?");
			// TW - if you don't know why I used a prepared statement instead of a normal statement please contact me for a talk
			// This both reduces the load on the database server and also helps fight against SQL injection attacks
		} catch (SQLException ex) {
			throw new RuntimeException("Cannot connect to database and prepare select statement.", ex);
			// TW - Runtime exceptions are used because the Lambda code does not except any initialisation exceptions
			// but will correctly handle runtime exceptions and error correctly with the message.
		} catch (InstantiationException ex) {
			throw new RuntimeException("Cannot load MySQL JDBC driver class - instantiation error.", ex);
		} catch (IllegalAccessException ex) {
			throw new RuntimeException("Cannot load MySQL JDBC driver class - illegal access.", ex);
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException("Cannot load MySQL JDBC driver class - class not found.", ex);
		}
	}

	// TW - this function is run multiple times within the Lambda container
	// It may not keep any state between runs, each run is independant of the others
	// It uses the global variables for database connections declared above so that it doesn't have to connect to the db each invocation
	@Override
	public OutputData handleRequest(InputData input, Context context) {
		context.getLogger().log("Input: " + input);
		OutputData outputData = new OutputData();
		outputData.setCurrencyFrom(input.getCurrencyFrom());
		outputData.setValueFrom(input.getValueFrom());
		outputData.setCurrencyTo(input.getCurrencyTo());

		try {
			select.clearParameters();
			select.setString(1, input.getCurrencyFrom());
			select.setString(2, input.getCurrencyTo());
			ResultSet rs = select.executeQuery();
			if (rs.next()) {
				float conversionRate = rs.getFloat(1);
				outputData.setValueTo(input.getValueFrom() * conversionRate);
			} else {
				outputData.setError("Cannot find a conversion rate from currency '"+input.getCurrencyFrom()+"' to currency '"+input.getCurrencyTo()+"'");
			}
		} catch (SQLException ex) {
			outputData.setError("SQL error while getting conversion rate from database.");
		}
		context.getLogger().log("OutputData: " + outputData);
		return outputData;

	}

}
