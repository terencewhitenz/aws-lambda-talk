package nz.co.hilites.test;

public class OutputData {

	private String currencyFrom;
	private float valueFrom;
	private String currencyTo;
	private float valueTo;
	private String error;

	public String getCurrencyFrom() {
		return currencyFrom;
	}
	public void setCurrencyFrom(String currencyFrom) {
		this.currencyFrom = currencyFrom;
	}
	public float getValueFrom() {
		return valueFrom;
	}
	public void setValueFrom(float valueFrom) {
		this.valueFrom = valueFrom;
	}
	public String getCurrencyTo() {
		return currencyTo;
	}
	public void setCurrencyTo(String currencyTo) {
		this.currencyTo = currencyTo;
	}
	public float getValueTo() {
		return valueTo;
	}
	public void setValueTo(float valueTo) {
		this.valueTo = valueTo;
	}
	@Override
	public String toString() {
		return "OutputData [" + (currencyFrom != null ? "currencyFrom=" + currencyFrom + ", " : "") + "valueFrom="
				+ valueFrom + ", " + (currencyTo != null ? "currencyTo=" + currencyTo + ", " : "") + "valueTo="
				+ valueTo + ", " + (error != null ? "error=" + error : "") + "]";
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

}
